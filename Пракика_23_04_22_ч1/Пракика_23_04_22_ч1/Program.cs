﻿using System;
using System.Linq;

namespace Пракика_23_04_22_ч1
{
    public class Convecter<T>
    {
        public T[] Value { get; set; }
        public int[] Weight { get; set; }
        public Convecter(T[] value, int[] weight)
        {
            Value = value;
            Weight = weight;
        }
        public T Convert()
        {
            Random rnd = new Random();
            int pool = Weight.Sum();
            int selected = rnd.Next(pool);
            int count = 0;
            for (int i = 0; i < Weight.Length; i++)
            {
                count = count + Weight[i];
                if (count > selected)
                    return Value[i];
            }
            return Value[0];
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Convecter<String> text = new Convecter<String>(new string[] { "str", "hi" }, new int[] { 1, 100 });
            for (int  i =0; i < 100; i++)
            Console.WriteLine($"Hello World! {text.Convert()}");
        }
    }
}
