﻿using System;
using System.Collections.Generic;

namespace Практика_26_03_2022_Часть2
{
    public class File
    {
        //public List<File> InFile = new 
        public List<File> InFile = new List<File>();
        public List<int> Data = new List<int>();

        public File(List<File> inFile, List<int> data)
        {
            InFile = inFile;
            Data = data;
        }
        public static void FileDisplay(File mainfile, int depth)
        {
            for (int i = 0; i < depth; i++)
                Console.Write("|---");
            Console.WriteLine($"Root {depth}");
            depth++;
            foreach (File file in mainfile.InFile)
                FileDisplay(file, depth);
            foreach (int i in mainfile.Data)
            {
                for (int i1 = -1; i1 < depth; i1++)
                    Console.Write("|---");
                Console.WriteLine($" {i}");
            }
        }
        public static void AidFile(File file, int[] path, int depth, int count)
        {
            if (depth == path.Length)
                for (int i = 1; i <= count; i++)
                    file.InFile.Add(new File(new List<File>(), new List<int>()));
            else
                AidFile(file.InFile[path[depth]], path, ++depth, count);
        }
        public static void AidInt(File file, int[] path, int depth, int[] data)
        {
            if (depth == path.Length)
                foreach (int i in data)
                    file.Data.Add(i);
            else
                AidInt(file.InFile[path[depth]], path, ++depth, data);
        }
        public static 

    }


    class Program
    {
        static void Main(string[] args)
        {
            File mainroot = new File(new List<File>(), new List<int> { 0, 2, 4 });
            File.AidFile(mainroot, new int[] {}, 0, 2); // ???
            File.AidInt(mainroot, new int[] { 1 }, 0, new int[] { 1, 2 });
            File.FileDisplay(mainroot, 0);

            //mainroot.InFile.Add(new File(new List<File>(), new List<int> { 0, 2 }));
            //mainroot.InFile[0].InFile.Add(new File(new List<File>(), new List<int> { 0, 5 }));
            //mainroot.InFile.Add(new File(new List<File>(), new List<int> { 6, 2 }));

            Console.WriteLine("Hello World!");
        }
    }
}
