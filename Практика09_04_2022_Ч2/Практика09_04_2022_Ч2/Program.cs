﻿using System;
using System.Text;

namespace Практика09_04_2022_Ч2
{
    class Program
    {
        public static void Error()
        {
            Console.WriteLine("Присутствует небуквеный символ");
            System.Environment.Exit(0);
        }
        public static (char, int) CheckLetter(StringBuilder newText, char save, int count, char letter)
        {
            if ('a' <= letter && letter <= 'z')
                if (letter == save)
                    count++;
                else
                {
                    AddText(newText, save, count);
                    count = 1;
                    save = letter;
                }
            else
            {
                Error();
            }
            return (save, count);
        }
        public static void AddText(StringBuilder newText, char save, int count)
        {
            newText.Append(save);
            if (count > 1)
                newText.Append($"{count}");
        }
        static void Main(string[] args)
        {
            string text = Console.ReadLine();
            var newText = new StringBuilder("");
            if (text == "")
                Error();
            int count = 1;
            if (!('a' <= text[0] && text[0] <= 'z'))
                Error();
            char save = text[0];
            for(int i = 1; i < text.Length; i++)
            {
                (save, count) = CheckLetter(newText, save, count, text[i]);
            }
            AddText(newText, save, count);
            Console.WriteLine($"{newText.ToString()}");
        }
    }
}
