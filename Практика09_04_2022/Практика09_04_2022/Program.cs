﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace Практика09_04_2022
{
    public class Person
    {
        public int Id { get; set; }
        public List<Slide> Slide = new List<Slide>();
        public Person(int id)
        {
            Id = id;
        }
    }
    public class Slide
    {
        public Theme Theme { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public Slide(Theme theme, DateTime date, DateTime time)
        {
            Theme = theme;
            Date = date;
            Time = time;
        }
    }
    public class Theme
    {
        public int Id { get; set; }
        /*
        public enum Type
        {
            theory,          Я не разобрался
            quiz,
            exercise
        }
        */
        public string Type { get; set; }
        public string Subject { get; set; }
        public Theme(int id, string type, string subject)
        {
            Id = id;
            Type = type;
            Subject = subject;
        }
    }

    class Program
    {
        public static DateTime StringToDate(string local)
        {
            DateTime Date;
            if (!DateTime.TryParseExact(local, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out Date))
                if (!DateTime.TryParseExact(local, "yyyy-mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out Date))
                    DateTime.TryParseExact(local, "yyyy-mm-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out Date);
            return Date;
        }
        public static DateTime StringToTime(string local)
        {
            DateTime Date;
            DateTime.TryParseExact(local, "HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out Date);
            return Date;
        }
        public static string FindPath(string name)
        {
            name = name + ".txt";
            string path = Path.Combine(Environment.CurrentDirectory, name);
            return name;
        }
        static void Main(string[] args)
        {
            string[] people = File.ReadAllLines(FindPath("people"));
            string[] themes = File.ReadAllLines(FindPath("slide"));
            List<Theme> Themes = new List<Theme>();
            List<Person> persons = new List<Person>();
            //List<Theme> Themess = from theme in themes
            //                      where 
            foreach (string theme in themes)
            {
                string[] data = theme.Split(";");
                Themes.Add(new Theme(Int32.Parse(data[0]), data[1], data[2]));
            }
        
            foreach (string human in people)
            {
                string[] data = human.Split(";");
                if (Int32.Parse(data[0]) == persons.Count)
                    persons.Add(new Person(persons.Count));
                persons[Int32.Parse(data[0])].Slide.Add(new Slide(Themes[Int32.Parse(data[1])], StringToDate(data[2]), StringToTime(data[3])));
            }
        }
    }
}
