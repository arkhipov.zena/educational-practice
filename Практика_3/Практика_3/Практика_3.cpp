﻿#include <iostream>
#include <stdio.h>
using namespace std;

int main()
{
	unsigned char a, b, y;
	char x;
	short c = 0, d = 0;

	scanf_s("%hhu %hhu", &a, &b);

	_asm
	{
		mov al, a;
		mov bl, b;
		add al, bl;
		jno sign_norm;
		mov c, 1;
	sign_norm:
		jnc unsign_norm;
		mov d, 1;
	unsign_norm:
		mov x, al;
		mov y, al;
	}

	cout << "a + b = " << (int)x << " (signed)" << endl;
 	cout << "a + b = " << (int)y << " (unsigned)" << endl;

	if (c == 1)
		cout << "overflow (signed)\n";
	else
		cout << "no overflow (signed)\n";

	if (d == 1)
		cout << "overflow (unsigned)\n";
	else
		cout << "no overflow (unsigned)\n";
}