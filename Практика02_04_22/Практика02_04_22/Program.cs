﻿using System;
using System.Collections.Generic;

namespace Практика02_04_22
{
    class Program //Первое задание
    {
        static void Main(string[] args) 
        {
            string text = Console.ReadLine();
            Stack<char> checc = new Stack<char>();
            bool flag = true;
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '[' || text[i] == '{' || text[i] == '(')
                    checc.Push(text[i]);
                else
                {
                    if (checc.Count < 0)
                    {
                        flag = false;
                        break;
                    }
                    if (checc.Peek() - text[i] == '[' - ']' || checc.Peek() - text[i] == '{' - '}' || checc.Peek() - text[i] == '(' - ')')
                        checc.Pop();
                    else
                    {
                        flag = false;
                        break;
                    }
                }
            }
            if (flag && checc.Count == 0)
                Console.WriteLine("Yes");
            else
                Console.WriteLine("No");
        }
    }
}
