﻿using System;
using System.Collections.Generic;

namespace Практика_19._03._22
{
    public static class Zeroing
    {
        public static void CheccZero(this List<(int, int)> coords, int[][] ara)
        {
            /*
            foreach (int[] mas in ara) 
                foreach (int num in mas)
                {
                    if (num == 0)
                        coords.Add()
                }
            */
            for (int i = 0; i < ara.Length; i++)
                for (int i1 = 0; i1 < ara[i].Length; i1++)
                {
                    if (ara[i][i1] == 0)
                        coords.Add((i, i1));
                }
        }
        public static void SettingZero(this int[][] ara, List<(int, int)> coords)
        {
            foreach ((int, int) pair in coords)
            {
                for (int i = 0; i < ara.Length; i++)
                    ara[i][pair.Item2] = 0;
                for (int i = 0; i < ara[pair.Item1].Length; i++)
                    ara[pair.Item1][i] = 0;
            }
        }

        public static void Zero(this int [][] ara)
        {
            List<(int, int)> coords = new List<(int, int)>();
            coords.CheccZero(ara);
            ara.SettingZero(coords);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            // List<(int, int)> coords = new List<(int, int)>();
            int[][] ara = new int[][]
            {
            new int[] { 8, 10, 0, 7, 0 },
            new int[] { 7, 4, 6, 3, 6, 10 },
            new int[] { 5, 6, 4, 6, 10}
            };
            ara.Zero();
            foreach (int[] mas in ara)
            {
                foreach (int num in mas)
                    Console.Write($"{num} ");
                Console.WriteLine();
            }

           // Console.WriteLine("Hello World!");
        }
    }
}
