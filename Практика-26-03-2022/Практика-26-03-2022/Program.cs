﻿using System;

namespace Практика_26_03_2022
{
    class Program
    { 
        static int[,] CreateChessBoard(int count)
        {
            int[,] chessBoard = new int[count, count];
            for (int i1 = 0; i1 < chessBoard.GetLength(0); i1++)
                for (int i2 = 0; i2 < chessBoard.GetLength(1); i2++)
                    chessBoard[i1, i2] = (i1 + i2) % 2;
            return chessBoard;
        }

        static void WriteChessBoard(int[,] chessBoard)
        {
            for (int i1 = 0; i1 < chessBoard.GetLength(0); i1++) 
            {
                for (int i2 = 0; i2 < chessBoard.GetLength(1); i2++)
                    Console.Write($"{chessBoard[i1,i2]} ");
                Console.WriteLine();
            }


        }
        static void Main(string[] args)
        {
            int count = Int32.Parse(Console.ReadLine());
            int[,] chessBoard = CreateChessBoard(count);
            WriteChessBoard(chessBoard);
        }
    }
}
