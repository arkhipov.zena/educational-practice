﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Практика02_04_2022
{
    class Program //Второе задание
    {
        static List<string> WordMade(int deep, int max, string[] alphabet, List<string> words)
        {
            List<string> newWords = new List<string>();
            deep++;
            if (deep == max)
                return words;
            foreach (string word in words)
                foreach (string letter in alphabet)
                    newWords.Add(word + letter);
            return WordMade(deep, max, alphabet, newWords);
        }
        static void Main(string[] args)
        {
            string[] alphabet = new string[] { "и", "к", "м", "т", "г"};
            List<string> allWord = WordMade(-1, 4, alphabet, new List<string> { "" });
            List<string> correctWord = new List<string>();
            string[] glossary = File.ReadAllLines(Path.Combine(Environment.CurrentDirectory, "russian.txt"), Encoding.UTF8);
            foreach (string myWord in allWord)
                foreach (string word in glossary) // Здесь основное веселье
                    if (myWord == word)
                        correctWord.Add(word);
            foreach (string word in correctWord)
                Console.WriteLine(word);
            // Весь этот шитпост вроде работает но с серьезной задержкой во времени(и возможно памяти при большом начальном массиве)
        }
    }
}
